# JEE integration exercise

A language trainer in the form of a quiz, which is built in JEE.

# Caveats

* Do not forget to create the database before deploying! You will also need to create the tables.
* Make sure to check the [[src/main/resources/META-INF/persistence.xml]] file for details on how you should configure the jdbc data source on your application server. The tests are set to expect [OpenJPA](http://openjpa.apache.org/)!
* The development was done on a machine with TomEE 8.0, Java JDK 8, and MySQL 8.
