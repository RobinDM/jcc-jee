<%@ page import="com.realdolmen.jcc.model.Word" %>
<%@ page import="java.util.List" %><%--<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>--%>
<html>
<head>
    <title>JCC Language Quiz</title>
    <jsp:include page="materialize.jsp"/>
</head>
<body>
<%--<div class="container">--%>
<jsp:include page="navbar.jsp"/>
<div>
    <div class="row">
        <div class="col s10 offset-s2">
            <h5>Translate from NL to EN</h5>
        </div>
        <%--        <div class="col s2 right">--%>
        <%--            Score: <%= request.getAttribute("score") %>/<%= request.getAttribute("total") %>--%>
        <%--        </div>--%>
    </div>
    <div class="row">
        <div class="col s8 offset-s2">
            How do we say "<%= request.getAttribute("challenge") %>" in English?
            <form method="post">
                <label>
                    English:
                    <input type="text" name="response"/>
                </label>
                <input type="submit" value="Check"/>
            </form>
        </div>
    </div>
    <% if ((int) request.getAttribute("score") > 0) { %>
    <div class="row">
        <div class="col s10 offset-s2">
            Score: <%= request.getAttribute("score") %>/<%= request.getAttribute("total") %>
        </div>
    </div>
    <div class="row">
        <div class="col s4 offset-s2">
            <table>
                <tr>
                    <th>Wrongs</th>
                </tr>
                <% for (String wrongs : (List<String>)request.getAttribute("wrongs")) { %>
                <tr>
                    <td>
                        <%= wrongs %>
                    </td>
                </tr>
                <% } %>
            </table>
        </div>
        <div class="col s4">
            <table>
                <tr>
                    <th>Rights</th>
                </tr>
                <% for (String rights : (List<String>)request.getAttribute("rights")) { %>
                <tr>
                    <td>
                        <%= rights %>
                    </td>
                </tr>
                <% } %>
            </table>
        </div>
    </div>
    <% } %>
</div>
</body>
</html>
