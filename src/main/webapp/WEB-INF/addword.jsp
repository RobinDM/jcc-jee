<%--
  Created by IntelliJ IDEA.
  User: robindm
  Date: 15/10/19
  Time: 19:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add word</title>
    <jsp:include page="materialize.jsp"/>
</head>
<body>
<jsp:include page="navbar.jsp"/>
<h1>Add new word:</h1>
<form method="post">
    <label>
        Dutch:
        <input type="text" name="dutch"/>
    </label>
    <label>
        English:
        <input type="text" name="english"/>
    </label>
    <input type="submit" value="Add"/>
</form>
</body>
</html>
