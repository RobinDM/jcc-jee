<%--
  Created by IntelliJ IDEA.
  User: robindm
  Date: 16/10/19
  Time: 09:19
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<nav>
    <div class="nav-wrapper">
        <a class="brand-logo" href="<%= request.getContextPath() %>">JCC language trainer (JEE integration exercise)</a>
        <ul id="nav-mobile" class="right hide-on-med-and-down">
            <li>
                <a href="dictionary">Dictionary</a>
            </li>
            <li>
                <a href="addword">Add word</a>
            </li>
        </ul>
    </div>
</nav>
