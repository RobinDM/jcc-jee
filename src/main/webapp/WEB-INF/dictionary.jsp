<%@ page import="com.realdolmen.jcc.model.Word" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: robindm
  Date: 15/10/19
  Time: 17:03
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Dictionary</title>
    <jsp:include page="materialize.jsp"/>
</head>
<body>
<jsp:include page="navbar.jsp"/>
<div class="row">
    <div class="col s10 offset-s2">
        <h4>
            Dictionary
        </h4>
    </div>
</div>
<div class="row">
    <div class="col s8 offset-s2">
        <table>
            <tr>
                <th>id</th>
                <th>Dutch</th>
                <th>English</th>
            </tr>
            <% for (Word word : (List<Word>) request.getAttribute("words")) { %>
            <tr>
                <td><%= word.getId() %>
                </td>
                <td><%= word.getDutch() %>
                </td>
                <td><%= word.getEnglish() %>
                </td>
            </tr>
            <% } %>
        </table>
    </div>
</div>
</body>
</html>
