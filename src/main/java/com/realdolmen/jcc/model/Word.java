package com.realdolmen.jcc.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@NamedQuery(name = Word.GET_ALL, query = "SELECT w FROM Word w")
public class Word implements Serializable {
    public static final String GET_ALL = "getAllWords";
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    private Long id;

    @Column(nullable = false)
    private String dutch;

    @Column(nullable = false)
    private String english;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDutch() {
        return dutch;
    }

    public void setDutch(String dutch) {
        this.dutch = dutch;
    }

    public String getEnglish() {
        return english;
    }

    public void setEnglish(String english) {
        this.english = english;
    }

    @Override
    public String toString() {
        return "Word{" + "id=" + id + ", dutch='" + dutch + '\'' + ", english='" + english + '\'' + '}';
    }
}
