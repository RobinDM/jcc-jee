package com.realdolmen.jcc.web;

import com.realdolmen.jcc.model.Word;
import com.realdolmen.jcc.persistence.WordRepository;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(urlPatterns = "/dictionary")
public class DictionaryServlet extends HttpServlet {

    @Inject
    private WordRepository wordRepository;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Word> words = wordRepository.getAllWords();
//        System.out.println("Found " + words.size() + " words.");
//        for (Word word : words) {
//            System.out.println(word);
//        }
        req.setAttribute("words", words);
        req.getRequestDispatcher("WEB-INF/dictionary.jsp")
           .forward(req, resp);
    }
}
