package com.realdolmen.jcc.web;

import com.realdolmen.jcc.model.Word;
import com.realdolmen.jcc.persistence.WordRepository;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/")
public class QuizServlet extends HttpServlet {

    @Inject
    private ScoreCounter scoreCounter;

    @Inject
    private WordRepository wordRepository;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Word randomWord = wordRepository.getRandomWord();
//        System.out.println("Got a random word: " + randomWord);
        scoreCounter.setChallenge(randomWord);
        req.setAttribute("challenge", randomWord.getDutch());
        req.setAttribute("wrongs", scoreCounter.getWrongs());
        req.setAttribute("rights", scoreCounter.getRights());
        req.setAttribute("score", scoreCounter.getScore());
        req.setAttribute("total", scoreCounter.getTotal());
        req.getRequestDispatcher("WEB-INF/index.jsp")
           .forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String response = req.getParameter("response");
        scoreCounter.validate(response);
        // force reload the page to display the score
        resp.sendRedirect(req.getContextPath());
    }
}
