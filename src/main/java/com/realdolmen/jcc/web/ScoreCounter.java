package com.realdolmen.jcc.web;

import com.realdolmen.jcc.model.Word;

import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@SessionScoped
public class ScoreCounter implements Serializable {
    private Word challenge;
    private List<String> wrongs = new ArrayList<>();
    private List<String> rights = new ArrayList<>();

    public ScoreCounter() {
        System.out.println("making a new score counter");
    }

    /**
     * Check to see if the supplied response corresponds to the stored challenge's English translation.
     * Will increment the total amount of tries by 1, and increment the score by 1 if the response matches the
     * translation exactly.
     * Will also reset challenge to null (you only get one try).
     *
     * @param response The value to compare to the challenge's English translation.
     */
    public void validate(String response) {
        if (challenge.getEnglish()
                     .equals(response)) {
            rights.add(challenge.getDutch() + " (" + response + ")");
        } else {
            wrongs.add(challenge.getDutch() + " (!" + response + ")");
        }
        challenge = null;
    }

    public void setChallenge(Word challenge) {
        this.challenge = challenge;
    }

    public int getScore() {
        return rights.size();
    }

    public int getTotal() {
        return rights.size() + wrongs.size();
    }

    public List<String> getWrongs() {
        return wrongs;
    }

    public List<String> getRights() {
        return rights;
    }
}
