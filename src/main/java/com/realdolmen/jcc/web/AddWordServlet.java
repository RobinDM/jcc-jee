package com.realdolmen.jcc.web;

import com.realdolmen.jcc.model.Word;
import com.realdolmen.jcc.persistence.WordRepository;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(urlPatterns = "/addword")
public class AddWordServlet extends HttpServlet {

    @Inject
    private WordRepository wordRepository;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        req.getRequestDispatcher("WEB-INF/addword.jsp")
           .forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String dutch = req.getParameter("dutch");
        String english = req.getParameter("english");
        Word word = new Word();
        word.setDutch(dutch);
        word.setEnglish(english);
        wordRepository.saveWord(word);
        resp.sendRedirect(req.getContextPath() + "/dictionary");
    }
}
