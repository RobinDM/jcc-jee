package com.realdolmen.jcc.persistence;

import com.realdolmen.jcc.model.Word;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

//@Named
//@Transactional
@Stateless
public class WordRepository {

    @PersistenceContext
    private EntityManager em;

    public List<Word> getAllWords() {
        return em.createNamedQuery(Word.GET_ALL, Word.class)
                 .getResultList();
    }

    public Word getRandomWord() {
        List<Word> words = new ArrayList<>(getAllWords());
        Collections.shuffle(words);
        return words.get(0);
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void saveWord(Word word) {
        em.persist(word);
    }
}
